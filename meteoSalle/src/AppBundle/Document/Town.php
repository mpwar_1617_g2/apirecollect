<?php

namespace AppBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use \DateTime;

/**
 * @MongoDB\Document
 */
class Town
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $comunityAutonome;

    /**
     * @MongoDB\Field(type="string")
     */
    private $province;

    /**
     * @MongoDB\Field(type="int")
     */
    private $code;

    /**
     * @MongoDB\Field(type="int")
     */
    private $dc;

    /**
     * @MongoDB\Field(type="string")
     */
    private $name;

    /**
     * @MongoDB\Field(type="date")
     */
    private $dateUpdate;

    public function getId()
    {
        return $this->id;
    }

    public function setComunityAutonome($comunityAutonome)
    {
        $this->comunityAutonome = $comunityAutonome;
    }
    public function getComunityAutonome()
    {
        return $this->comunityAutonome;
    }

    public function setProvince($province)
    {
        $this->province = $province;

    }
    public function getProvince()
    {
        return $this->province;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }
    public function getCode()
    {
        return $this->code;
    }

    public function setDc($dc)
    {
        $this->dc = $dc;
    }
    public function getDc()
    {
        return $this->dc;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    }
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    public function __construct()
    {
    }

    static function create():Town{
        $town = new Town();
        return $town;
    }

    public function getCodeWithProvincie():string{
        $townCode = str_pad($this->code, 3, '0', STR_PAD_LEFT);
        $townProvince = str_pad($this->province, 2, '0', STR_PAD_LEFT);
        return $townProvince.$townCode;
    }
}
