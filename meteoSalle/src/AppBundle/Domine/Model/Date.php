<?php

namespace AppBundle\Domine\Model;
use \InvalidArgumentException;
use \DateTime;

class Date
{
    private $date;

    public function __construct
    (
        string $date
    )
    {
        if (!Date::validate($date))
        {
            throw new InvalidArgumentException(sprintf('"%s" invalid Date.', $date));
        }

        $this->date = new DateTime($date);
    }

    static function validate(string $date):bool
    {

        if (DateTime::createFromFormat('Y-m-d H:i:s', $date)){
            return true;
        }
        if (DateTime::createFromFormat('Y-m-d', $date)){
            return true;
        }
        if (new DateTime ($date)){
            return true;
        }
        return false;
    }

    public function __toString():string
    {
        return $this->date->format('Y-m-d H:i:s');
    }

    public function toDateTime():DateTime
    {
        return new DateTime($this->__toString());
    }

    public function equals(DateTime $date):bool
    {
        return ($this->date === $date);
    }

    public function format(string $format):string
    {
        return $this->date->format($format);
    }
}