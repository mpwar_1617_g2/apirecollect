<?php

namespace AppBundle\Domine\Service;

interface PredictionForDays
{
    public function __invoke(string $town);
}