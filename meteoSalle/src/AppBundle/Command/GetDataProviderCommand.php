<?php
namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Document\Town as TownMongoDb;
use AppBundle\Document\Province as ProvinceMongoDb;
use AppBundle\Document\History as HistoryMongoDb;
use AppBundle\Document\AemetStation;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use AppBundle\Application\Service\GetValuesOfHistoryAemetUseCase;
use GuzzleHttp\Client as ApiClient;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputOption;
use AppBundle\Application\Service\FindPredictionDayUseCase;
use \InvalidArgumentException;
use AppBundle\Domine\Model\Date;
use AppBundle\Controller\Repository\RepositoryMongoDb;

class GetDataProviderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:GetDataProviderCommand')
            ->setDescription('Get data from providers.')
            ->setHelp('not helper...')
            ->addOption('historybegin', 'B', InputOption::VALUE_REQUIRED, 'Date yyyy-mm-dd to begin import of data history. The date end default is today', false)
            ->addOption('historyend', 'E', InputOption::VALUE_REQUIRED, 'Date yyyy-mm-dd to end import of data history. The date begin default it is the same as historyend.', false)
            ->addOption('history', 'H', InputOption::VALUE_NONE, 'Import of data history.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repositoryMongoDb = new RepositoryMongoDb($this->getContainer()->get('doctrine_mongodb'));

        $dateBegin = $this->getDateBegin($input);
        $dateEnd = $this->getDateEnd($input);

        if ($dateBegin > $dateEnd){
            throw new InvalidArgumentException(sprintf('invalid dates. The date begin can not be greater than the date end.'));
        }

        $findHistory = $input->getOption('history');
        if ($findHistory) {
            $this->ImportHistoryToAEMET($repositoryMongoDb, $output, $dateBegin,  $dateEnd);
            return;
        }

        $this->getPredictionForDaysToProvinces($repositoryMongoDb, $output);
        $this->getPredictionForDays($repositoryMongoDb, $output);
    }

    private function isCallDiferentDateBegin(InputInterface $input):bool {
        if ($input->getOption('historybegin')){
            return true;
        }
        return false;
    }

    private function isCallDiferentDateEnd(InputInterface $input):bool {
        if ($input->getOption('historyend')){
            return true;
        }
        return false;
    }

    private function getPredictionForDaysToProvinces(RepositoryMongoDb $repositoryMongoDb, OutputInterface $output){
        $apiClient = new ApiClient();

        $output->writeln([
            '' ,
            'Get Data to Prediction Days For Provinces' ,
            '==================================' ,
        ]);

        $output->writeln(['    searching provinces ...']);
        $provinces = $repositoryMongoDb->findAllProvincesForPrediction();

        $provincesNumbers = count($provinces);
        $output->writeln(['    '.$provincesNumbers.' provinces finded']);

        if ($provincesNumbers === 0){
            $output->writeln([
                '' ,
                '    End' ,
                '==================================' ,
                '' ,
            ]);

            return;
        }

        foreach ($provinces as $provinceForTownCapital){
            $towns[] = $repositoryMongoDb->findTownByProvinceAndCode($provinceForTownCapital->getProvince(), $provinceForTownCapital->getTownCapital());
        }

        $progressBar = new ProgressBar($output, $provincesNumbers);
        $findPredictionDayUseCase = new FindPredictionDayUseCase($progressBar, $repositoryMongoDb, $apiClient);
        $townsNotFound = $findPredictionDayUseCase($towns,true);

        if ($townsNotFound !== ''){
            $output->writeln([
                '',
                '    provinces not found : '.$townsNotFound,
            ]);
        }

        $output->writeln([
            '' ,
            '    End' ,
            '==================================' ,
            '' ,
        ]);
    }

    private function getPredictionForDays(RepositoryMongoDb $repositoryMongoDb, OutputInterface $output){
        $apiClient = new ApiClient();

        $output->writeln([
            '' ,
            'Get Data to Prediction Days For Towns' ,
            '==================================' ,
        ]);

        $output->writeln(['    searching towns ...']);
        $towns = $repositoryMongoDb->findTownsForPrediction();

        $townsNumbers = count($towns);
        $output->writeln(['    '.$townsNumbers.' towns finded']);

        $progressBar = new ProgressBar($output, $townsNumbers);
        $findPredictionDayUseCase = new FindPredictionDayUseCase($progressBar, $repositoryMongoDb, $apiClient);
        $townsNotFound = $findPredictionDayUseCase($towns, false);

        if ($townsNotFound !== ''){
            $output->writeln([
                '',
                '    towns not found : '.$townsNotFound,
            ]);
        }

        $output->writeln([
            '' ,
            '    End' ,
            '==================================' ,
            '' ,
        ]);
    }

    private function ImportHistoryToAEMET(RepositoryMongoDb $repositoryMongoDb, OutputInterface $output, Date $dateBegin, Date $dateEnd)
    {
        $output->writeln([
            '' ,
            'Get Data Provider History From AEMET' ,
            '==================================' ,
        ]);

        $apiClient = new ApiClient();

        $aemetStation = AemetStation::create();
        $town = TownMongoDb::create($repositoryMongoDb);
        $getValuesOfHistoryAemetUseCase = new GetValuesOfHistoryAemetUseCase($repositoryMongoDb, $apiClient, $aemetStation, $town);

        $output->writeln(['    Searching data ...']);
        $dataHistory = $getValuesOfHistoryAemetUseCase($dateBegin, $dateEnd);

        if ($dataHistory === NULL){
            $output->writeln([
                "Data noy found" ,
                '==================================',
            ]);
            return;
        }

        $historyNumbers = count($dataHistory);
        $output->writeln([
            '    saving data ...' ,
            "    $historyNumbers register to save"
        ]);
        $progress = new ProgressBar($output ,$historyNumbers);

        foreach ($dataHistory as $item) {
            $date = new Date ($item['fecha']);

            $historyDTO = $getValuesOfHistoryAemetUseCase->getItemToArray($date, $item);

            $repositoryMongoDb->removeHistoryByDay($historyDTO->getDate(), $historyDTO->getProvider(), $historyDTO->getTown());

            $historyMongoDb = HistoryMongoDb::create();
            $historyMongoDb->setToDTO($historyDTO);
            $repositoryMongoDb->saveHistory($historyMongoDb);

            $progress->advance();
        }

        $countHistoryNumbers = count($dataHistory);
        $progress->finish();

        $output->writeln([
            '' ,
            "    import $countHistoryNumbers registers" ,
            "Finished" ,
            '==================================' ,
            ''
        ]);
    }

    private function getDateBegin(InputInterface $input):Date
    {
        if (!$this->isCallDiferentDateBegin($input)) {
            return $this->getDateDefault();
        }

        $date = new Date($input->getOption('historybegin'));
        $dateBegin = new Date($date);

        return $dateBegin;
    }

    private function getDateEnd(InputInterface $input):Date
    {
        if (!$this->isCallDiferentDateEnd($input)) {
            return $this->getDateDefault();
        }

        $date = new Date($input->getOption('historyend'));
        $dateEnd = new Date($date);

        if ($dateEnd > $this->getDateDefault()){
            return $this->getDateDefault();
        }

        return $dateEnd;
    }

    private function getDateDefault():Date{
        $date = date("Y-m-d");
        $date = strtotime($date."- 4 days");
        return new Date(date('Y-m-d H:i:s', $date));
    }
}