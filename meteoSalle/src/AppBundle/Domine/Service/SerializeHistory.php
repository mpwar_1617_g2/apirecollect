<?php

namespace AppBundle\Domine\Service;
use \DateTime;
use \DateInterval;
use \Doctrine\ODM\MongoDB\Cursor;
use \AppBundle\Domine\Infrastructure\IHistories;

class SerializeHistory
{
    private $repository;

    public function __construct()
    {
    }

    public function __invoke(IHistories $histories, string $codeProvinceTown, DateTime $date):array
    {
        if ($histories->count() === 0)
        {
            return array("status" => 404 ,
                         "description" => "Data not found");
        }

        $max_temperatureAvg = 0;
        $min_temperatureAvg = 0;
        $base_temperature_hourAvg = new DateTime('00:00:00');
        $max_temperature_hourAvg = new DateTime('00:00:00');
        $min_temperature_hourAvg = new DateTime('00:00:00');
        $precipitationAvg = 0;
        $registersTemperatureCount = 0;
        $registersTemperatureHourCount = 0;
        $registersPrecipitationCount = 0;
        $totalPrecipitation = 0;

        foreach ($histories as $history) {
            if ($history->getTemperatureMaximum() !== NULL){
                $registersTemperatureCount++;

                $max_temperatureAvg += $history->getTemperatureMaximum();
                $min_temperatureAvg += $history->getTemperatureMinimum();
            }

            if ($history->getTemperatureMaximumHour() !== NULL){
                $registersTemperatureHourCount++;

                $hourToSum2 = new DateTime($history->getTemperatureMaximumHour().":00");
                $max_temperature_hourAvg = $this->sumHours($max_temperature_hourAvg, $hourToSum2);

                $hourToSum3 = new DateTime($history->getTemperatureMinimumHour().":00");
                $min_temperature_hourAvg = $this->sumHours($min_temperature_hourAvg, $hourToSum3);
            }

            if ($history->getRainFall() !== NULL) {
                $registersPrecipitationCount++;

                $precipitationAvg += $history->getRainFall();
            }
        }

        if ($precipitationAvg === 0 || $registersPrecipitationCount === 0) {
            $totalPrecipitation = 0;
        }
        else
        {
            $totalPrecipitation = $precipitationAvg / $registersPrecipitationCount;
        }

        $historyFinded = array(
            "status" => 200,
            "town_code" => $codeProvinceTown,
            "date" => $date->format('Y-m-d'),
            "max_temperature" => number_format($max_temperatureAvg / $registersTemperatureCount,2, '.', ''),
            "min_temperature" => number_format($min_temperatureAvg / $registersTemperatureCount,2, '.', ''),
            "max_temperature_hour" =>  gmdate("H:i", ($max_temperature_hourAvg->format('U') - $base_temperature_hourAvg->format('U')) / $registersTemperatureHourCount++),
            "min_temperature_hour" =>  gmdate("H:i", ($min_temperature_hourAvg->format('U') - $base_temperature_hourAvg->format('U')) / $registersTemperatureHourCount++),
            "precipitation" => number_format($totalPrecipitation,2, '.', '')
        );

        return $historyFinded;
    }

    private function sumHours(DateTime $hourToSum1, DateTime $hourToSum2):DateTime{
        $hourToSum = date('H' , date_timestamp_get($hourToSum2));
        $minuteToSum = date('i' , date_timestamp_get($hourToSum2));


        $hour = str_pad($hourToSum, 2, '0', STR_PAD_LEFT);
        $minute = str_pad($minuteToSum, 2, '0', STR_PAD_LEFT);
        $time = 'PT'.$hour.'H'.$minute.'S';

        $interval = new DateInterval($time);
        $hourToSum1->add($interval);
        return $hourToSum1;
    }
}