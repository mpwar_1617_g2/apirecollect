<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use AppBundle\Domine\Model\HistoryDTO;
use AppBundle\Domine\Model\Date;

/**
 * @MongoDB\Document
 */
class History
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $provider;

    /**
     * @MongoDB\Field(type="string")
     */
    private $country;

    /**
     * @MongoDB\Field(type="string")
     */
    private $town;

    /**
     * @MongoDB\Field(type="string")
     */
    private $province;
    
    /**
     * @MongoDB\Field(type="date")
     */
    private $date;

    /**
     * @MongoDB\Field(type="float")
     */
    private $altitude;

    /**
     * @MongoDB\Field(type="float")
     */
    private $rainFall;

    /**
     * @MongoDB\Field(type="float")
     */
    private $temperatureMedium;

    /**
     * @MongoDB\Field(type="float")
     */
    private $temperatureMinimum;

    /**
     * @MongoDB\Field(type="float")
     */
    private $temperatureMaximum;

    /**
     * @MongoDB\Field(type="string")
     */
    private $temperatureMinimumHour;

    /**
     * @MongoDB\Field(type="string")
     */
    private $temperatureMaximumHour;

    /**
     * @MongoDB\Field(type="int")
     */
    private $windDirection;

    /**
     * @MongoDB\Field(type="float")
     */
    private $windVelocityMedium;

    /**
     * @MongoDB\Field(type="float")
     */
    private $windGust;

    /**
     * @MongoDB\Field(type="string")
     */
    private $windGustHour;

    /**
     * @MongoDB\Field(type="float")
     */
    private $pressureMinimum;

    /**
     * @MongoDB\Field(type="float")
     */
    private $pressureMaximum;

    /**
     * @MongoDB\Field(type="string")
     */
    private $pressureMinimumHour;

    /**
     * @MongoDB\Field(type="string")
     */
    private $pressureMaximumHour;

    /**
     * @MongoDB\Field(type="float")
     */
    private $sun;

    /**
     * @MongoDB\Field(type="date")
     */
    private $dateCreated;

    public function getId()
    {
        return $this->id;
    }

    public function setProvider($provider)
    {
        $this->provider = $provider;
    }
    public function getProvider()
    {
        return $this->provider;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }
    public function getDate()
    {
        return $this->date;
    }

    public function setTemperatureMedium($temperatureMedium)
    {
        $this->temperatureMedium = $temperatureMedium;
    }
    public function getTemperatureMedium()
    {
        return $this->temperatureMedium;
    }

    public function setTown($town)
    {
        $this->town = $town;
    }
    public function getTown()
    {
        return $this->town;
    }

    public function setProvince($province)
    {
        $this->province = $province;
    }
    public function getProvince()
    {
        return $this->province;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }
    public function getCountry()
    {
        return $this->country;
    }

    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;
    }
    public function getAltitude()
    {
        return $this->altitude;
    }

    public function setRainFall($rainFall)
    {
        $this->rainFall = $rainFall;
    }
    public function getRainFall()
    {
        return $this->rainFall;
    }

    public function setTemperatureMinimum($temperatureMinimum)
    {
        $this->temperatureMinimum = $temperatureMinimum;
    }
    public function getTemperatureMinimum()
    {
        return $this->temperatureMinimum;
    }

    public function setTemperatureMaximum($temperatureMaximum)
    {
        $this->temperatureMaximum = $temperatureMaximum;
    }
    public function getTemperatureMaximum()
    {
        return $this->temperatureMaximum;
    }

    public function setTemperatureMinimumHour($temperatureMinimumHour)
    {
        $this->temperatureMinimumHour = $temperatureMinimumHour;
    }
    public function getTemperatureMinimumHour()
    {
        return $this->temperatureMinimumHour;
    }

    public function setTemperatureMaximumHour($temperatureMaximumHour)
    {
        $this->temperatureMaximumHour = $temperatureMaximumHour;
    }
    public function getTemperatureMaximumHour()
    {
        return $this->temperatureMaximumHour;
    }

    public function setWindDirection($windDirection)
    {
        $this->windDirection = $windDirection;
    }
    public function getWindDirection()
    {
        return $this->windDirection;
    }

    public function setWindVelocityMedium($windVelocityMedium)
    {
        $this->windVelocityMedium = $windVelocityMedium;
    }
    public function getWindVelocityMedium()
    {
        return $this->windVelocityMedium;
    }

    public function setWindGust($windGust)
    {
        $this->windGust = $windGust;
    }
    public function getWindGust()
    {
        return $this->windGust;
    }

    public function setWindGustHour($windGustHour)
    {
        $this->windGustHour = $windGustHour;
    }
    public function getWindGustHour()
    {
        return $this->windGustHour;
    }

    public function setPressureMinimum($pressureMinimum)
    {
        $this->pressureMinimum = $pressureMinimum;
    }
    public function getPressureMinimum()
    {
        return $this->pressureMinimum;
    }

    public function setPressureMaximum($pressureMaximum)
    {
        $this->pressureMaximum = $pressureMaximum;
    }
    public function getPressureMaximum()
    {
        return $this->pressureMaximum;
    }

    public function setPressureMinimumHour($pressureMinimumHour)
    {
        $this->pressureMinimumHour = $pressureMinimumHour;
    }
    public function getPressureMinimumHour()
    {
        return $this->pressureMinimumHour;
    }

    public function setPressureMaximumHour($pressureMaximumHour)
    {
        $this->pressureMaximumHour = $pressureMaximumHour;
    }
    public function getPressureMaximumHour()
    {
        return $this->pressureMaximumHour;
    }

    public function setSun($sun)
    {
        $this->sun = $sun;
    }
    public function getSun()
    {
        return $this->sun;
    }

    public function getDateCreated(){
        return $this->dateCreated;
    }
    public function setDateCreated(Date $dateCreated){
        $this->dateCreated = $dateCreated;
    }

    public function __construct()
    {
        $dateCreated = new Date("now");
        $this->dateCreated = $dateCreated->toDateTime();
    }

    static function create():History{
        $history = new History();
        return $history;
    }

    public function setToDTO(HistoryDTO $historyDTO)
    {
        $this->provider = $historyDTO->getProvider();
        $this->town = $historyDTO->getTown();
        $this->province = $historyDTO->getProvince();
        $this->country = $historyDTO->getCountry();
        $this->date = $historyDTO->getDate()->toDateTime();
        $this->altitude = $historyDTO->getAltitude();
        $this->rainFall = $historyDTO->getRainFall();
        $this->temperatureMedium = $historyDTO->getTemperatureMedium();
        $this->temperatureMinimum = $historyDTO->getTemperatureMinimum();
        $this->temperatureMaximum = $historyDTO->getTemperatureMaximum();
        $this->temperatureMinimumHour = $historyDTO->getTemperatureMinimumHour();
        $this->temperatureMaximumHour = $historyDTO->getTemperatureMaximumHour();
        $this->windDirection = $historyDTO->getWindDirection();
        $this->windVelocityMedium = $historyDTO->getWindVelocityMedium();
        $this->windGust = $historyDTO->getWindGust();
        $this->windGustHour = $historyDTO->getWindGustHour();
        $this->pressureMinimum = $historyDTO->getPressureMinimum();
        $this->pressureMaximum = $historyDTO->getPressureMaximum();
        $this->pressureMinimumHour = $historyDTO->getPressureMinimumHour();
        $this->pressureMaximumHour = $historyDTO->getPressureMaximumHour();
        $this->sun = $historyDTO->getSun();
    }

    static public function removeAll($repository){
        $mongoDbRepository = $repository->getManager();

        $mongoDbRepositoryes = $repository
            ->getManager()
            ->createQueryBuilder('AppBundle:History')
            ->getQuery()
            ->execute();

        foreach ($mongoDbRepositoryes as $historyMongoDb) {
            $mongoDbRepository->remove($historyMongoDb);
            $mongoDbRepository->flush();
        }
    }
}
