<?php

namespace AppBundle\Domine\Model;

use AppBundle\Domine\Model\HistoryDTO;
use AppBundle\Domine\Model\Date;

interface History
{
    public function __invoke(Date $dateBegin,
                             Date $dateEnd);

    public function getItemToArray(Date $date,
                                   Array $historyInput):HistoryDTO;
}
