<?php


namespace AppBundle\Application\Service;
use AppBundle\Domine\Service\PredictionForDaysAemet;
use AppBundle\Document\Province;
use AppBundle\Document\Town;
use GuzzleHttp\Client as ApiClient;
use \DateTime;
use \AppBundle\Domine\Infrastructure\IRepository;

class FindPredictionTownUseCase
{
    private $apiClient;
    private $repository;

    public function __construct(IRepository $repository, ApiClient $apiClient)
    {
        $this->repository = $repository;
        $this->apiClient = $apiClient;
    }

    public function __invoke(Town $town, bool $updateProvince)
    {
        $townCode = $town->getCodeWithProvincie();
        $predictionForDaysAemet = new PredictionForDaysAemet ($this->apiClient, $this->repository);
        $dateActual = new DateTime (date("Y-m-d"));

        if (!$predictionForDaysAemet($townCode)){
            return false;
        };

        $town->setDateUpdate($dateActual);
        $this->repository->saveTown($town);

        if ($updateProvince) {
            $province = $this->repository->findProvinceByCode($town->getProvince());
            $this->repository->saveProvince($province);
        }

        return true;
    }
}