<?php
/**
 * Created by PhpStorm.
 * User: racso
 * Date: 12/08/17
 * Time: 9:34
 */

namespace AppBundle\Domine\Service;
use \DateTime;
use \AppBundle\Domine\Model\SkyStatus;
use \Doctrine\ODM\MongoDB\Cursor;
use \AppBundle\Domine\Infrastructure\IPredictions;

class SerializePredictions
{
    public function __construct()
    {
    }

    public function __invoke(IPredictions $predictions):array
    {
        if ($predictions->count() === 0){
            return array("status" => 404,
                "description" => "Data not found");
        }

        $actualTown = "";
        $predictionFinded = array();
        foreach ($predictions as $prediction) {
            if ($actualTown !== $prediction->getTown()){
                if ($actualTown !== ""){
                    $predictionFinded[] = $this->getPredictionForTown(
                        $town,
                        $predictionDate ,
                        $updateDate ,
                        $maxTemperatureAvg ,
                        $minTemperatureAvg ,
                        $temperatureEarlyMorningAvg ,
                        $temperatureMorningAvg ,
                        $temperatureAfternoonAvg ,
                        $temperatureEveningAvg ,
                        $rainProbabilityEarlyMorningAvg ,
                        $rainProbabilityMorningAvg ,
                        $rainProbabilityAfternoonAvg ,
                        $rainProbabilityEveningAvg ,
                        $skyEarlyMorning ,
                        $skyMorning ,
                        $skyAfternoon ,
                        $skyEvening ,
                        $maxWindAvg);
                }

                $actualTown = $prediction->getTown();
                $maxTemperatureAvg = 0;
                $minTemperatureAvg = 0;
                $temperatureEarlyMorningAvg = 0;
                $temperatureMorningAvg = 0;
                $temperatureAfternoonAvg = 0;
                $temperatureEveningAvg = 0;
                $rainProbabilityEarlyMorningAvg = 0;
                $rainProbabilityMorningAvg = 0;
                $rainProbabilityAfternoonAvg = 0;
                $rainProbabilityEveningAvg = 0;
                $maxWindAvg = 0;
                $temperatureCounter = 0;
                $temperaturePartCounter = 0;
                $rainPartCounter = 0;
                $windPartCounter = 0;
            }
            $town = $prediction->getTown();
            $predictionDate = $prediction->getDate();
            $updateDate = $prediction->getDateUpdate();

            if ($prediction->getTemperatureMaximum() !== NULL){
                $temperatureCounter++;
                $maxTemperatureAvg += $prediction->getTemperatureMaximum();
                $minTemperatureAvg += $prediction->getTemperatureMinimum();
            }

            if ($prediction->getTemperatureEarlyMorning() !== NULL){
                $temperaturePartCounter++;
                $temperatureEarlyMorningAvg += $prediction->getTemperatureEarlyMorning();
                $temperatureMorningAvg += $prediction->getTemperatureMorning();
                $temperatureAfternoonAvg += $prediction->getTemperatureAfternoon();
                $temperatureEveningAvg += $prediction->getTemperatureEvening();
            }

            if ($prediction->getRainProbabilityEarlyMorning() !== NULL){
                $rainPartCounter++;
                $rainProbabilityEarlyMorningAvg += $prediction->getRainProbabilityEarlyMorning();
                $rainProbabilityMorningAvg += $prediction->getRainProbabilityMorning();
                $rainProbabilityAfternoonAvg += $prediction->getRainProbabilityAfternoon();
                $rainProbabilityEveningAvg += $prediction->getRainProbabilityEvening();
            }

            if ($prediction->getWindMaximum() !== NULL){
                $windPartCounter++;
                $maxWindAvg += $prediction->getWindMaximum();
            }

            $skyEarlyMorning =  $prediction->getSkyStatusEarlyMorning();
            $skyMorning =  $prediction->getSkyStatusMorning();
            $skyAfternoon = $prediction->getSkyStatusAfternoon();
            $skyEvening = $prediction->getSkyStatusEvening();
        }

        if ($temperatureCounter !== 0){
            $maxTemperature = $maxTemperatureAvg / $temperatureCounter;
            $minTemperature = $minTemperatureAvg / $temperatureCounter;
        }else{
            $maxTemperature = 0;
            $minTemperature = 0;
        }

        if ($temperaturePartCounter !== 0){
            $temperatureEarlyMorning = $temperatureEarlyMorningAvg / $temperaturePartCounter;
            $temperatureMorning = $temperatureEarlyMorningAvg / $temperaturePartCounter;
            $temperatureAfternoon = $temperatureAfternoonAvg / $temperaturePartCounter;
            $temperatureEvening = $temperatureEveningAvg / $temperaturePartCounter;
        }else{
            $temperatureEarlyMorning = 0;
            $temperatureMorning = 0;
            $temperatureAfternoon = 0;
            $temperatureEvening = 0;
        }

        if ($rainPartCounter !== 0){
            $rainProbabilityEarlyMorning = $rainProbabilityEarlyMorningAvg / $rainPartCounter;
            $rainProbabilityMorning = $rainProbabilityMorningAvg / $rainPartCounter;
            $rainProbabilityAfternoon = $rainProbabilityAfternoonAvg / $rainPartCounter;
            $rainProbabilityEvening = $rainProbabilityEveningAvg / $rainPartCounter;
        }else{
            $rainProbabilityEarlyMorning = 0;
            $rainProbabilityMorning = 0;
            $rainProbabilityAfternoon = 0;
            $rainProbabilityEvening = 0;
        }

        if ($windPartCounter !== 0){
            $maxWind = $maxWindAvg / $windPartCounter;
        }else{
            $maxWind = 0;
        }

        $predictionFinded[] = $this->getPredictionForTown($town ,
            $predictionDate ,
            $updateDate ,
            $maxTemperature,
            $minTemperature,
            $temperatureEarlyMorning,
            $temperatureMorning,
            $temperatureAfternoon,
            $temperatureEvening,
            $rainProbabilityEarlyMorning,
            $rainProbabilityMorning,
            $rainProbabilityAfternoon,
            $rainProbabilityEvening,
            $skyEarlyMorning ,
            $skyMorning ,
            $skyAfternoon ,
            $skyEvening ,
            $maxWind);

        return $predictionFinded;
    }

    private function getPredictionForTown(string $town,
                                          DateTime $predictionDate,
                                          DateTime $updateDate,
                                          $maxTemperatureAvg,
                                          $minTemperatureAvg,
                                          $temperatureEarlyMorningAvg,
                                          $temperatureMorningAvg,
                                          $temperatureAfternoonAvg,
                                          $temperatureEveningAvg,
                                          $rainProbabilityEarlyMorningAvg,
                                          $rainProbabilityMorningAvg,
                                          $rainProbabilityAfternoonAvg,
                                          $rainProbabilityEveningAvg,
                                          SkyStatus $skyStatusEarlyMorning,
                                          SkyStatus $skyStatusMorning,
                                          SkyStatus $skyStatusAfternoon,
                                          SkyStatus $skyStatusEvening,
                                          $maxWindAvg):array
    {
        $predictionForTown = array(
            "status" => 200 ,
            "town_code" => $town,
            "prediction_date" => $predictionDate->format('Y-m-d') ,
            "import_date" => $updateDate->format('Y-m-d H:i:s') ,
            "max_temperature" => number_format($maxTemperatureAvg ,2 ,'.' ,'') ,
            "min_temperature" => number_format($minTemperatureAvg ,2 ,'.' ,'') ,
            "rain_probability" => array(
                "early_morning" => number_format($rainProbabilityEarlyMorningAvg ,0 ,'.' ,'') ,
                "morning" => number_format($rainProbabilityMorningAvg, 0 ,'.' ,'') ,
                "afternoon" => number_format($rainProbabilityAfternoonAvg,0 ,'.' ,'') ,
                "evening" => number_format($rainProbabilityEveningAvg ,0 ,'.' ,'')
            ) ,
            "max_wind" => number_format($maxWindAvg ,2 ,'.' ,'') ,
            "temperature" => array(
                "early_morning" => number_format($temperatureEarlyMorningAvg ,0 ,'.' ,'') ,
                "morning" => number_format($temperatureMorningAvg ,0 ,'.' ,'') ,
                "afternoon" => number_format($temperatureAfternoonAvg ,0 ,'.' ,'') ,
                "evening" => number_format($temperatureEveningAvg ,0 ,'.' ,'')
            ) ,
            "sky_status" => array(
                "early_morning" => $skyStatusEarlyMorning->__toString() ,
                "morning" => $skyStatusMorning->__toString() ,
                "afternoon" => $skyStatusAfternoon->__toString() ,
                "evening" => $skyStatusEvening->__toString()
            )
        );

        return $predictionForTown;
    }
}