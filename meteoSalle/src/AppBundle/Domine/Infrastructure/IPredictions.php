<?php

namespace AppBundle\Domine\Infrastructure;
use AppBundle\Document\PredictionDay;

interface IPredictions
{
    public function count():int;

    public function rewind();

    public function valid():bool;

    public function current():PredictionDay;

    public function next();
}