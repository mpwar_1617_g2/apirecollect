<?php

namespace AppBundle\Application\Service;
use AppBundle\Document\Town;
use GuzzleHttp\Client as ApiClient;
use AppBundle\Document\AemetStation;
use AppBundle\Domine\Infrastructure\AemetDataProvider;
use AppBundle\Domine\Model\History;
use AppBundle\Domine\Model\HistoryDTO;
use AppBundle\Domine\Model\Date;
use AppBundle\Domine\Infrastructure\IRepository;

class GetValuesOfHistoryAemetUseCase
    extends AemetDataProvider
    implements History
{
    private $repository;
    private $aemetStation;
    private $town;
    private $dateBegin;
    private $dateEnd;

    public function __construct(
        IRepository $repository,
        ApiClient $apiClient,
        AemetStation $aemetStation,
        Town $town
    ) {
        parent::__construct($apiClient);
        $this->repository = $repository;
        $this->aemetStation = $aemetStation;
        $this->town = $town;
    }

    public function __invoke(Date $dateBegin, Date $dateEnd)
    {
        $this->dateBegin = $dateBegin;
        $this->dateEnd = $dateEnd;

        $data = $this->getValuesOfprovider();

        if ($data === ''){
            return false;
        }

        return $data;
    }

    public function getItemToArray(Date $date,
                                   Array $historyInput):HistoryDTO{
        $historyDTO = new HistoryDTO(AemetDataProvider::NAME, $date);

        $aemetStationCode = $historyInput['indicativo'];
        $aemetStationName = $historyInput['nombre'];
        $town = $this->findTown($aemetStationCode, $aemetStationName);

        if ($town === NULL) {
            $townCode = "00000";
            $province = "";
        }
        else{
            $townCode = $town->getCodeWithProvincie();
            $province = $town->getProvince();
        }

        $historyDTO->setTown($townCode);
        $historyDTO->setProvince($province);
        $historyDTO->setCountry('ES');

        if (array_key_exists("altitud", $historyInput)) {
            $historyDTO->setAltitude($historyInput['altitud']);
        }

        $rainFall = $this->validateToFloat($historyInput, 'prec');
        if ($rainFall !== NULL){
            $historyDTO->setRainFall($rainFall);
        }

        $temperatureMedium = $this->validateToFloat($historyInput, 'tmed');
        if ($temperatureMedium !== NULL){
            $historyDTO->setTemperatureMedium($temperatureMedium);
        }

        $temperatureMinimum = $this->validateToFloat($historyInput, 'tmin');
        if ($temperatureMinimum !== NULL){
            $historyDTO->setTemperatureMinimum($temperatureMinimum);
        }

        $temperatureMaximum = $this->validateToFloat($historyInput, 'tmax');
        if ($temperatureMaximum !== NULL){
            $historyDTO->setTemperatureMaximum($temperatureMaximum);
        }

        $temperatureMinimumHour = $this->validateToHour($historyInput, 'horatmin');
        if ($temperatureMinimumHour !== NULL){
            $historyDTO->setTemperatureMinimumHour($temperatureMinimumHour);
        }

        $temperatureMaximumHour = $this->validateToHour($historyInput, 'horatmax');
        if ($temperatureMaximumHour !== NULL){
            $historyDTO->setTemperatureMaximumHour($temperatureMaximumHour);
        }

        $windDirection = $this->validateToInt($historyInput, 'dir');
        if ($windDirection !== NULL){
            $historyDTO->setWindDirection($windDirection);
        }

        $windVelocityMedium = $this->validateToFloat($historyInput, 'velmedia');
        if ($windVelocityMedium !== NULL){
            $historyDTO->setWindVelocityMedium($windVelocityMedium);
        }

        $windGust = $this->validateToFloat($historyInput, 'racha');
        if ($windGust !== NULL){
            $historyDTO->setWindGust($windGust);
        }

        $windGustHour = $this->validateToHour($historyInput, 'horaracha');
        if ($windGustHour !== NULL){
            $historyDTO->setWindGustHour($windGustHour);
        }

        $pressureMinimum = $this->validateToFloat($historyInput, 'presMin');
        if ($pressureMinimum !== NULL){
            $historyDTO->setPressureMinimum($pressureMinimum);
        }

        $pressureMaximum = $this->validateToFloat($historyInput, 'presMax');
        if ($pressureMaximum !== NULL){
            $historyDTO->setPressureMaximum($pressureMaximum);
        }

        $pressureMinimumHour = $this->validateToHour($historyInput, 'horaPresMin');
        if ($pressureMinimumHour !== NULL){
            $historyDTO->setPressureMinimumHour($pressureMinimumHour);
        }

        $pressureMaximumHour = $this->validateToHour($historyInput, 'horaPresMax');
        if ($pressureMaximumHour !== NULL){
            $historyDTO->setPressureMaximumHour($pressureMaximumHour);
        }

        $sun = $this->validateToFloat($historyInput, 'sol');
        if ($sun !== NULL){
            $historyDTO->setSun($sun);
        }

        return $historyDTO;
    }

    protected function getAction():string{
        $dateBeginFormated = $this->formatDate($this->dateBegin);
        $dateEndFormated = $this->formatDate($this->dateEnd);

        return "valores/climatologicos/diarios/datos/fechaini/".$dateBeginFormated."T00:00:00UTC/fechafin/".$dateEndFormated."T00:00:00UTC/todasestaciones";
    }

    private function findTown(string $aemetStationCode, string $aemetStationName)
    {
        $aemetStation = $this->repository->findAemetStationByCode($aemetStationCode);

        if ($aemetStation === NULL) {
            $aemetStation = $this->repository->findAemetStationByName($aemetStationName);
        }

        if ($aemetStation === NULL) {
            $town = 'undefined';
        } else {
            $town = $aemetStation->getTown();
        }

        $townMongoDb = $this->repository->findTownByName($town);

        return $townMongoDb;
    }
}