<?php

namespace AppBundle\Tests\Dependencies\TestCase;
use GuzzleHttp\Client as ClientApi;
use PHPUnit\Framework\TestCase;

abstract class FunctionalTestCase extends TestCase
{
    protected function ApiConnection(): ClientApi
    {
        return new ClientApi();
    }
}