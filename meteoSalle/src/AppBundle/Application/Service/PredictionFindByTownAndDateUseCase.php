<?php

namespace AppBundle\Application\Service;
use \DateTime;
use AppBundle\Domine\Service\SerializePredictions;
use AppBundle\Domine\Infrastructure\IRepository as Repository;

class PredictionFindByTownAndDateUseCase
{
    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(int $province, int $townCode, DateTime $date):array
    {
        $townSearch =  $this->repository->findTownByProvinceAndCode($province, $townCode);
        $predictions = $this->repository->findPredictionDayWithTownAndDate($townSearch->getCodeWithProvincie(), $date);
        $serializePredictions = new SerializePredictions();
        $serializedPredictions = $serializePredictions->__invoke($predictions);
        return $serializedPredictions;
    }
}