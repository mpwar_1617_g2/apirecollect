<?php

namespace AppBundle\Application\Service;
use GuzzleHttp\Client as ApiClient;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Application\Service\FindPredictionTownUseCase;
use AppBundle\Domine\Infrastructure\IRepository;
class FindPredictionDayUseCase
{
    private $progressBar;
    private $repository;
    private $apiClient;

    public function __construct
    (
        ProgressBar $progressBar,
        IRepository $repository,
        ApiClient $apiClient
    )
    {
        $this->progressBar = $progressBar;
        $this->repository = $repository;
        $this->apiClient = $apiClient;
    }

    public function __invoke($towns, bool $updateProvince):string
    {
        $townsNotFound = '';
        $townsNotFound = $townsNotFound.$this->getFromAemet($towns, $updateProvince);

        return $townsNotFound;
    }

    private function getFromAemet($towns, bool $updateProvince):string{
        $townsNotFound = '';
        $separator = '';
        $findPredictionTownUseCase = new FindPredictionTownUseCase($this->repository, $this->apiClient);

        foreach ($towns as $town) {
            $isTownFinded = $findPredictionTownUseCase($town, $updateProvince);

            if (!$isTownFinded){
                $townsNotFound = $townsNotFound.$separator.$town->getCodeWithProvincie();
                $separator = ', ';
            };

            $this->progressBar->advance();
            sleep(5);
        }

        return $townsNotFound;
    }
}