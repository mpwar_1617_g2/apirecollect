<?php

namespace AppBundle\Domine\Infrastructure;
use GuzzleHttp\Client as ApiClient;
use \AppBundle\Domine\Model\Date;

abstract class AemetDataProvider implements DataProvider
{
    const NAME = 'AEMET';
    const KEY_APY = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzaGluaWdhbWlzaGluenVAZ21haWwuY29tIiwianRpIjoiYzZiZjlkODktNWZlMS00YzhhLWEwNjgtODdhNzg0NDUxZjFiIiwiZXhwIjoxNTA2MTg2OTU0LCJpc3MiOiJBRU1FVCIsImlhdCI6MTQ5ODQxMDk1NCwidXNlcklkIjoiYzZiZjlkODktNWZlMS00YzhhLWEwNjgtODdhNzg0NDUxZjFiIiwicm9sZSI6IiJ9.ql6JT6rIUQIDMy3xm8rGyIgpuiaSejOO_K-gKiJOjMA';
    const URL_BASE = "https://opendata.aemet.es/opendata/api/";

    private $apiClient;

    abstract protected function getAction():string;

    public function getName(): string
    {
        return AemetDataProvider::NAME;
    }

    public function __construct
    (
        ApiClient $apiClient
    )
    {
        $this->apiClient = $apiClient;
    }

    protected function getValuesOfprovider()
    {
        $urlIntermediate = $this->getUrlIntermediate();

        if ($urlIntermediate === NULL){
            return;
        }

        $data = $this->getJsonOfCall($urlIntermediate);

        if ($data === NULL) {
            $data = $this->getDataOptionCurl($urlIntermediate);
        }

        if ($data === NULL){
            return;
        }

        if (array_key_exists('estado' ,$data)) {
            return;
        }

        return $data;
    }

    private function getUrlIntermediate(){
        $aemetAction = $this->getAction();
        $url = AemetDataProvider::URL_BASE . $aemetAction . "/?api_key=" . AemetDataProvider::KEY_APY;
        $data = $this->getJsonOfCall ($url);

        if ($data === NULL) {
            $data = $this->getDataOptionCurl($url);
        }

        if (array_key_exists('datos', $data)){
            return $data['datos'];
        }

        return;
    }

    private function getJsonOfCall(string $url){
        $res = $this->apiClient->request('GET',
            $url,
            ['verify' => false]
        );

        $result = $res->getBody();
        $json = json_decode($result->getContents(),true);

        return $json;
    }

    private function getDataOptionCurl(string $url){
        $curl = curl_init();
        curl_setopt_array($curl ,array(
            CURLOPT_URL => $url ,
            CURLOPT_SSL_VERIFYPEER => false ,
            CURLOPT_RETURNTRANSFER => true ,
            CURLOPT_ENCODING => "" ,
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $response = iconv('UTF-8' ,'UTF-8//IGNORE' ,utf8_encode($response));
        $json = json_decode($response ,true);
        return $json;
    }

    protected function formatDate(Date $dateIn):string{
        return $dateIn->format('Y-m-d');
    }

    protected function validateToFloat(Array $historyInput, string $key){
        if (!array_key_exists($key, $historyInput)) {
            return;
        }

        $value = str_replace(',', '.', $historyInput[$key]);
        if (!is_numeric($value)){
            return;
        }

        return $value;
    }

    protected function validateToHour(Array $historyInput, string $key){
        if (!array_key_exists($key, $historyInput)) {
            return;
        }

        $value = $historyInput[$key];

        $pattern="/^([0-1][0-9]|[2][0-3])[\:]([0-5][0-9])$/";
        if(preg_match($pattern, $value)) {
            return $value;
        }

        $pattern="/^([0-1][0-9]|[2][0-3])$/";
        if(preg_match($pattern, $value)) {
            return $value.":00";
        }

        return "00:00";
    }

    protected function validateToInt(Array $historyInput, string $key){
        if (!array_key_exists($key, $historyInput)) {
            return;
        }

        $value = $historyInput[$key];
        if (!is_numeric($value)){
            return;
        }

        return $value;
    }
}