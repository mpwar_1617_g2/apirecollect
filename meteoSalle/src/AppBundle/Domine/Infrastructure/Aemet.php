<?php

namespace AppBundle\Domine\Infrastructure;
use GuzzleHttp\Client as ApiClient;
use \DateTime;

abstract class Aemet
{
    const KEY_APY = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzaGluaWdhbWlzaGluenVAZ21haWwuY29tIiwianRpIjoiYzZiZjlkODktNWZlMS00YzhhLWEwNjgtODdhNzg0NDUxZjFiIiwiZXhwIjoxNTA2MTg2OTU0LCJpc3MiOiJBRU1FVCIsImlhdCI6MTQ5ODQxMDk1NCwidXNlcklkIjoiYzZiZjlkODktNWZlMS00YzhhLWEwNjgtODdhNzg0NDUxZjFiIiwicm9sZSI6IiJ9.ql6JT6rIUQIDMy3xm8rGyIgpuiaSejOO_K-gKiJOjMA';
    const URL_BASE = "https://opendata.aemet.es/opendata/api/";
    const PROVIDER = 'AEMET';

    private $apiClient;

    public function __construct
    (
        ApiClient $apiClient
    )
    {
        $this->apiClient = $apiClient;
    }

    protected function getJsonOfCall(string $url){
        $res = $this->apiClient->request('GET',
            $url,
            ['verify' => false]
        );

        $result = $res->getBody();
        $json = json_decode($result->getContents(),true);

        return $json;
    }

    protected function getDataOptionCurl(string $url){
        $curl = curl_init();
        curl_setopt_array($curl ,array(
            CURLOPT_URL => $url ,
            CURLOPT_SSL_VERIFYPEER => false ,
            CURLOPT_RETURNTRANSFER => true ,
            CURLOPT_ENCODING => "" ,
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $response = iconv('UTF-8' ,'UTF-8//IGNORE' ,utf8_encode($response));
        $json = json_decode($response ,true);
        return $json;
    }

    protected function formatDate(DateTime $dateIn):string{
        //$date   = new DateTime();
        //$dateOut = $date->createFromFormat('d/m/Y', $dateIn);
        return $dateIn->format('Y-m-d');
    }

    protected function validateToFloat(Array $historyInput, string $key){
        if (!array_key_exists($key, $historyInput)) {
            return;
        }

        $value = str_replace(',', '.', $historyInput[$key]);
        if (!is_numeric($value)){
            return;
        }

        return $value;
    }

    protected function validateToHour(Array $historyInput, string $key){
        if (!array_key_exists($key, $historyInput)) {
            return;
        }

        $value = $historyInput[$key];

        return $value;
    }

    protected function validateToInt(Array $historyInput, string $key){
        if (!array_key_exists($key, $historyInput)) {
            return;
        }

        $value = $historyInput[$key];
        if (!is_numeric($value)){
            return;
        }

        return $value;
    }
}