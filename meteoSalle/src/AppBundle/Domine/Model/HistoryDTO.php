<?php

namespace AppBundle\Domine\Model;
use AppBundle\Domine\Model\Date;

class HistoryDTO
{
    private $provider;
    private $town;
    private $province;
    private $country;
    private $date;
    private $altitude;
    private $rainFall;
    private $temperatureMedium;
    private $temperatureMinimum;
    private $temperatureMaximum;
    private $temperatureMinimumHour;
    private $temperatureMaximumHour;
    private $windDirection;
    private $windVelocityMedium;
    private $windGust;
    private $windGustHour;
    private $pressureMinimum;
    private $pressureMaximum;
    private $pressureMinimumHour;
    private $pressureMaximumHour;
    private $sun;

    public function __construct(string $provider,
                                Date $date)
    {
        $this->provider = $provider;
        $this->date = $date;
    }

    public function getProvider():string
    {
        return $this->provider;
    }

    public function getDate():Date
    {
        return $this->date;
    }

    public function setTemperatureMedium(float $temperatureMedium)
    {
        $this->temperatureMedium = $temperatureMedium;
        return $this;
    }
    public function getTemperatureMedium()
    {
        return $this->temperatureMedium;
    }

    public function setTown(string $town)
    {
        $this->town = $town;
        return $this;
    }
    public function getTown():string
    {
        return $this->town;
    }

    public function setProvince(string $province)
    {
        $this->province = $province;
        return $this;
    }
    public function getProvince():string
    {
        return $this->province;
    }

    public function setCountry(string $country)
    {
        $this->country = $country;
    }
    public function getCountry():string
    {
        return $this->country;
    }

    public function setAltitude(float $altitude)
    {
        $this->altitude = $altitude;
    }
    public function getAltitude()
    {
        return $this->altitude;
    }

    public function setRainFall(float $rainFall)
    {
        $this->rainFall = $rainFall;
    }
    public function getRainFall()
    {
        return $this->rainFall;
    }

    public function setTemperatureMinimum(float $temperatureMinimum)
    {
        $this->temperatureMinimum = $temperatureMinimum;
    }
    public function getTemperatureMinimum()
    {
        return $this->temperatureMinimum;
    }

    public function setTemperatureMaximum(float $temperatureMaximum)
    {
        $this->temperatureMaximum = $temperatureMaximum;
    }
    public function getTemperatureMaximum()
    {
        return $this->temperatureMaximum;
    }

    public function setTemperatureMinimumHour(string $temperatureMinimumHour)
    {
        $this->temperatureMinimumHour = $temperatureMinimumHour;
    }
    public function getTemperatureMinimumHour()
    {
        return $this->temperatureMinimumHour;
    }

    public function setTemperatureMaximumHour(string $temperatureMaximumHour)
    {
        $this->temperatureMaximumHour = $temperatureMaximumHour;
    }
    public function getTemperatureMaximumHour()
    {
        return $this->temperatureMaximumHour;
    }

    public function setWindDirection(int $windDirection)
    {
        $this->windDirection = $windDirection;
    }
    public function getWindDirection()
    {
        return $this->windDirection;
    }

    public function setWindVelocityMedium(float $windVelocityMedium)
    {
        $this->windVelocityMedium = $windVelocityMedium;
    }
    public function getWindVelocityMedium()
    {
        return $this->windVelocityMedium;
    }

    public function setWindGust(float $windGust)
    {
        $this->windGust = $windGust;
    }
    public function getWindGust()
    {
        return $this->windGust;
    }

    public function setWindGustHour(string $windGustHour)
    {
        $this->windGustHour = $windGustHour;
    }
    public function getWindGustHour()
    {
        return $this->windGustHour;
    }

    public function setPressureMinimum(float $pressureMinimum)
    {
        $this->pressureMinimum = $pressureMinimum;
    }
    public function getPressureMinimum()
    {
        return $this->pressureMinimum;
    }

    public function setPressureMaximum(float $pressureMaximum)
    {
        $this->pressureMaximum = $pressureMaximum;
    }
    public function getPressureMaximum()
    {
        return $this->pressureMaximum;
    }

    public function setPressureMinimumHour(string $pressureMinimumHour)
    {
        $this->pressureMinimumHour = $pressureMinimumHour;
    }
    public function getPressureMinimumHour()
    {
        return $this->pressureMinimumHour;
    }

    public function setPressureMaximumHour(string $pressureMaximumHour)
    {
        $this->pressureMaximumHour = $pressureMaximumHour;
    }
    public function getPressureMaximumHour()
    {
        return $this->pressureMaximumHour;
    }

    public function setSun(float $sun)
    {
        $this->sun = $sun;
    }
    public function getSun()
    {
        return $this->sun;
    }
}