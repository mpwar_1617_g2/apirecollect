<?php

namespace AppBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use \DateTime;
use AppBundle\Domine\Model\SkyStatus;

/**
 * @MongoDB\Document
 */
class PredictionHour
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="int")
     */
    private $hour;

    /**
     * @MongoDB\Field(type="string")
     */
    private $skyStatus;

    /**
     * @MongoDB\Field(type="int")
     */
    private $rainProbability;

    /**
     * @MongoDB\Field(type="int")
     */
    private $stormProbability;

    /**
     * @MongoDB\Field(type="int")
     */
    private $snowProbability;

    /**
     * @MongoDB\Field(type="float")
     */
    private $temperature;

    /**
     * @MongoDB\Field(type="float")
     */
    private $termicSensation;

    /**
     * @MongoDB\Field(type="int")
     */
    private $relativeHumidity;

    /**
     * @MongoDB\Field(type="string")
     */
    private $windDirection;

    /**
     * @MongoDB\Field(type="float")
     */
    private $windVelocity;

    /**
     * @MongoDB\Field(type="float")
     */
    private $windGustMaximum;

    /**
     * @MongoDB\Field(type="date")
     */
    private $dateUpdate;

    public function getId()
    {
        return $this->id;
    }

    public function setHour($hour)
    {
        $this->hour = $hour;
    }
    public function getHour()
    {
        return $this->hour;
    }

    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;
    }
    public function getTemperature()
    {
        return $this->temperature;
    }

    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    }
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    public function setSkyStatus(SkyStatus $skyStatus)
    {
        $this->skyStatus = $skyStatus;
    }
    public function getSkyStatus():SkyStatus
    {
        return $this->skyStatus;
    }

    public function setRainProbability($rainProbability)
    {
        $this->rainProbability = $rainProbability;
    }

    public function getRainProbability()
    {
        return $this->rainProbability;
    }

    public function setStormProbability($stormProbability)
    {
        $this->stormProbability = $stormProbability;
    }
    public function getStormProbability()
    {
        return $this->stormProbability;
    }

    public function setSnowProbability($snowProbability)
    {
        $this->snowProbability = $snowProbability;
    }
    public function getSnowProbability()
    {
        return $this->snowProbability;
    }

    public function setTermicSensation($termicSensation)
    {
        $this->termicSensation = $termicSensation;
    }
    public function getTermicSensation()
    {
        return $this->termicSensation;
    }

    public function setRelativeHumidity($relativeHumidity)
    {
        $this->relativeHumidity = $relativeHumidity;
    }
    public function getRelativeHumidity()
    {
        return $this->relativeHumidity;
    }

    public function setWindDirection($windDirection)
    {
        $this->windDirection = $windDirection;
    }
    public function getWindDirection()
    {
        return $this->windDirection;
    }

    public function setWindVelocity($windVelocity)
    {
        $this->windVelocity = $windVelocity;
    }
    public function getWindVelocity()
    {
        return $this->windVelocity;
    }

    public function setWindGustMaximum($windGustMaximum)
    {
        $this->windGustMaximum = $windGustMaximum;
    }
    public function getWindGustMaximum()
    {
        return $this->windGustMaximum;
    }
}
