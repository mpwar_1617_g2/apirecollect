<?php

namespace AppBundle\Domine\Service;
use AppBundle\Domine\Infrastructure\AemetDataProvider;
use AppBundle\Document\PredictionDay;
use AppBundle\Domine\Service\PredictionForDays;
use AppBundle\Domine\Model\SkyStatus;
use AppBundle\Document\Town;
use GuzzleHttp\Client as ApiClient;
use \DateTime;
use \AppBundle\Domine\Infrastructure\IRepository;

class PredictionForDaysAemet
    extends AemetDataProvider
    implements PredictionForDays
{
    private $town;
    private $repository;

    public function __construct(ApiClient $apiClient,
                                IRepository $repository)
    {
        parent::__construct($apiClient);
        $this->repository = $repository;
    }

    public function __invoke(string $town):bool
    {
        $this->town = $town;
        $data = $this->getValuesOfprovider();

        if ($data === ''){
            return false;
        }

        $predictionForDays = $data[0]['prediccion']['dia'];
        $numberOfDias = count($predictionForDays);

        for ($actualDay = 0; $actualDay < $numberOfDias; $actualDay++) {
            $this->getDayOfProvider($town, $predictionForDays[$actualDay]);
        }

        return true;
    }

    protected function getAction(): string
    {
        return "prediccion/especifica/municipio/diaria/".$this->town;
    }

    private function getDayOfProvider(string $town, Array $dayOfProvider){
        $date = new DateTime ($dayOfProvider['fecha']);
        $provider = AemetDataProvider::NAME;

        $country = 'ES';
        $dateActual = new DateTime (date("Y-m-d"));
        if ($dateActual > $date){
            return;
        }

        $this->repository->removePredictionDay($date, $provider, $town);
        $predictionDay = new PredictionDay();
        $predictionDay->setDate($date);
        $predictionDay->setProvider($provider);
        $predictionDay->setTown($town);
        $predictionDay->setCountry($country);
        $predictionDay->setTemperatureMinimum($this->getTemperatureMinimum($dayOfProvider));
        $predictionDay->setTemperatureMaximum($this->getTemperatureMaximum($dayOfProvider));

        $preparedTemperatureParts = $this->prepareTemperatureParts($dayOfProvider);
        if (count($preparedTemperatureParts) > 0){
            $predictionDay->setTemperatureEarlyMorning($this->getTemperatureEarlyMorning($preparedTemperatureParts));
            $predictionDay->setTemperatureMorning($this->getTemperatureMorning($preparedTemperatureParts));
            $predictionDay->setTemperatureAfternoon($this->getTemperatureAfternoon($preparedTemperatureParts));
            $predictionDay->setTemperatureEvening($this->getTemperatureEvening($preparedTemperatureParts));
        }

        $preparedRainProbabilityParts = $this->prepareRainProbabilityParts($dayOfProvider);
        $predictionDay->setRainProbabilityEarlyMorning($this->getRainProbabilityEarlyMorning($preparedRainProbabilityParts));
        $predictionDay->setRainProbabilityMorning($this->getRainProbabilityMorning($preparedRainProbabilityParts));
        $predictionDay->setRainProbabilityAfternoon($this->getRainProbabilityAfternoon($preparedRainProbabilityParts));
        $predictionDay->setRainProbabilityEvening($this->getRainProbabilityEvening($preparedRainProbabilityParts));

        $preparedSkyStatusParts = $this->prepareSkyStatusParts($dayOfProvider);
        $predictionDay->setSkyStatusEarlyMorning($this->getSkyStatusEarlyMorning($preparedSkyStatusParts));
        $predictionDay->setSkyStatusMorning($this->getSkyStatusMorning($preparedSkyStatusParts));
        $predictionDay->setSkyStatusAfternoon($this->getSkyStatusAfternoon($preparedSkyStatusParts));
        $predictionDay->setSkyStatusEvening($this->getSkyStatusEvening($preparedSkyStatusParts));

        $predictionDay->setWindMaximum($this->getWindMaximum($dayOfProvider));
        $predictionDay->setDateUpdate(new DateTime("now"));

        $this->repository->savePrediction($predictionDay);
    }

    private function getTemperatureMaximum(Array $dayOfProvider):float{
        return $dayOfProvider['temperatura']['maxima'];
    }

    private function getTemperatureMinimum(Array $dayOfProvider):float{
        return $dayOfProvider['temperatura']['minima'];
    }

    private function prepareTemperatureParts(Array $dayOfProvider):Array{
        return $dayOfProvider['temperatura']['dato'];
    }

    private function getTemperatureEarlyMorning(Array $preparedTemperatureParts):float{
        return $preparedTemperatureParts[0]['value'];
    }

    private function getTemperatureMorning(Array $preparedTemperatureParts):float{
        return $preparedTemperatureParts[1]['value'];
    }

    private function getTemperatureAfternoon(Array $preparedTemperatureParts):float{
        return $preparedTemperatureParts[2]['value'];
    }

    private function getTemperatureEvening(Array $preparedTemperatureParts):float{
        return $preparedTemperatureParts[3]['value'];
    }

    private function prepareRainProbabilityParts(Array $dayOfProvider):Array{
        $rainProbability = $dayOfProvider['probPrecipitacion'];
        $rainProbabilityFinally = array();

        switch (count($rainProbability)) {
            case 1:
                $rainProbabilityFinally[] = $rainProbability[0]['value'];
                $rainProbabilityFinally[] = $rainProbability[0]['value'];
                $rainProbabilityFinally[] = $rainProbability[0]['value'];
                $rainProbabilityFinally[] = $rainProbability[0]['value'];
                break;
            case 3:
                $rainProbabilityFinally[] = $rainProbability[1]['value'];
                $rainProbabilityFinally[] = $rainProbability[1]['value'];
                $rainProbabilityFinally[] = $rainProbability[2]['value'];
                $rainProbabilityFinally[] = $rainProbability[2]['value'];
                break;
            case 7:
                $rainProbabilityFinally[] = $rainProbability[3]['value'];
                $rainProbabilityFinally[] = $rainProbability[4]['value'];
                $rainProbabilityFinally[] = $rainProbability[5]['value'];
                $rainProbabilityFinally[] = $rainProbability[6]['value'];
                break;
        }

        return $rainProbabilityFinally;
    }

    private function getRainProbabilityEarlyMorning(Array $preparedRainProbabilityParts):float{
        return $preparedRainProbabilityParts[0];
    }

    private function getRainProbabilityMorning(Array $preparedRainProbabilityParts):float{
        return $preparedRainProbabilityParts[1];
    }

    private function getRainProbabilityAfternoon(Array $preparedRainProbabilityParts):float{
        return $preparedRainProbabilityParts[2];
    }

    private function getRainProbabilityEvening(Array $preparedRainProbabilityParts):float{
        return $preparedRainProbabilityParts[3];
    }

    private function prepareSkyStatusParts(Array $dayOfProvider):Array{
        $skyStatus = $dayOfProvider['estadoCielo'];
        $skyStatusFinally = array();

        switch (count($skyStatus)) {
            case 1:
                $skyStatusFinally[] = $skyStatus[0]['descripcion'];
                $skyStatusFinally[] = $skyStatus[0]['descripcion'];
                $skyStatusFinally[] = $skyStatus[0]['descripcion'];
                $skyStatusFinally[] = $skyStatus[0]['descripcion'];
                break;
            case 3:
                $skyStatusFinally[] = $skyStatus[1]['descripcion'];
                $skyStatusFinally[] = $skyStatus[1]['descripcion'];
                $skyStatusFinally[] = $skyStatus[2]['descripcion'];
                $skyStatusFinally[] = $skyStatus[2]['descripcion'];
                break;
            case 7:
                $skyStatusFinally[] = $skyStatus[3]['descripcion'];
                $skyStatusFinally[] = $skyStatus[4]['descripcion'];
                $skyStatusFinally[] = $skyStatus[5]['descripcion'];
                $skyStatusFinally[] = $skyStatus[6]['descripcion'];
                break;
        }

        return $skyStatusFinally;
    }

    private function getSkyStatusEarlyMorning(Array $preparedSkyStatusParts):SkyStatus{
        if (SkyStatus::validate($preparedSkyStatusParts[0])){
            return new SkyStatus($preparedSkyStatusParts[0]);
        }

        return new SkyStatus($this->getSkyStatusMorning($preparedSkyStatusParts));
    }

    private function getSkyStatusMorning(Array $preparedSkyStatusParts):SkyStatus{
        if (SkyStatus::validate($preparedSkyStatusParts[1])){
            return new SkyStatus($preparedSkyStatusParts[1]);
        }

        return new SkyStatus($this->getSkyStatusAfternoon($preparedSkyStatusParts));
    }

    private function getSkyStatusAfternoon(Array $preparedSkyStatusParts):SkyStatus{
        if (SkyStatus::validate($preparedSkyStatusParts[2])){
            return new SkyStatus($preparedSkyStatusParts[2]);
        }

        return new SkyStatus($this->getSkyStatusEvening($preparedSkyStatusParts));
    }

    private function getSkyStatusEvening(Array $preparedSkyStatusParts):SkyStatus{
        return new SkyStatus($preparedSkyStatusParts[3]);
    }

    private function getWindMaximum(Array $dayOfProvider):int{
        $wind = $dayOfProvider['viento'];
        $windFinally = array();

        switch (count($wind)) {
            case 1:
                $windFinally[] = $wind[0]['velocidad'];
                break;
            case 3:
                $windFinally[] = $wind[1]['velocidad'];
                $windFinally[] = $wind[2]['velocidad'];
                break;
            case 7:
                $windFinally[] = $wind[3]['velocidad'];
                $windFinally[] = $wind[4]['velocidad'];
                $windFinally[] = $wind[5]['velocidad'];
                $windFinally[] = $wind[6]['velocidad'];
                break;
        }

        $windValue = 0;
        foreach ($windFinally as $windPart){
            if ($windPart > $windValue){
                $windValue = $windPart;
            }
        }

        return $windValue;
    }
}