<?php

namespace AppBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class AemetStation
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $code;

    /**
     * @MongoDB\Field(type="string")
     */
    private $name;

    /**
     * @MongoDB\Field(type="string")
     */
    private $town;

    /**
     * @MongoDB\Field(type="string")
     */
    private $province;

    public function getId()
    {
        return $this->id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }
    public function getCode()
    {
        return $this->code;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setTown($town)
    {
        $this->town = $town;
    }
    public function getTown()
    {
        return $this->town;
    }

    public function setProvince($province)
    {
        $this->province = $province;
    }
    public function getProvince()
    {
        return $this->province;
    }

    public function __construct()
    {
    }

    static function create():AemetStation{
        $aemetStation = new AemetStation();
        return $aemetStation;
    }

//    public function saveItemToArray(Array $aemetStationIn){
//        $mongoDbRepository = $this->repository->getManager();
//
//        $this->code = $aemetStationIn['indicativo'];
//        $this->name = $aemetStationIn['nombre'];
//        $this->town = $aemetStationIn['indsinop'];
//        $this->province = $aemetStationIn['provincia'];
//
//        $mongoDbRepository->persist($this);
//        $mongoDbRepository->flush();
//    }
//
//    static public function removeAll($repository){
//        $mongoDbRepository = $repository->getManager();
//
//        $aemetStations = $repository
//            ->getManager()
//            ->createQueryBuilder('AppBundle:AemetStation')
//            ->getQuery()
//            ->execute();
//
//        foreach ($aemetStations as $aemetStation) {
//            $mongoDbRepository->remove($aemetStation);
//            $mongoDbRepository->flush();
//        }
//    }
}
