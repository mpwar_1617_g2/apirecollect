<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class BaseController extends FOSRestController
{
    protected function normalize($object, array $context = []) {
        return $this->get('serializer.default')->normalize($object, 'json', $context);
    }

    protected function denormalize($collection, $type, array $context = []) {
        return $this->get('serializer.default')->denormalize($collection, $type, $context);
    }
}