# Readme.md
### Provisionamiento de máquinas virtuales con vagrant y ansible

---

**Máquinas en vagrand**

* apicollect
* repository

**Roles en ansible**

* common
* composer
* mongoDb
* mysql
* php

**playbook's en ansible**

* playbook_webserver
* playbook_backEnd
* playbook_frontEnd
* playbook_reposiroty

**Uso**

* Desplegar máquinas virtuales
```html
vagrant up
o
vagrant up apicollect
o
vagrant up repository
```

* conectarse a web mediante ssh para proceder al provisionamiento con ansible
```html
vagrant ssh apicollect
```

* Provionar entornos. En función del entorno que se desee provisionar, ejecutar:
```html
ansible-playbook ansible/playbook_backEnd.yml -i ansible/inventory/hosts -e entorno=devel
o
ansible-playbook ansible/playbook_frontEnd.yml -i ansible/inventory/hosts -e entorno=devel
o
ansible-playbook ansible/playbook_repository.yml -i ansible/inventory/hosts -e entorno=devel
o
ansible-playbook ansible/playbook_webserver.yml -i ansible/inventory/hosts -e entorno=devel
o
ansible-playbook ansible/playbook_backEnd.yml -i ansible/inventory/hosts -e entorno=aws
```

* Desplegar aplicación en servidor.*
Acceder a la ruta /deployment/production/playbooks/web
```html
sudo ansible-playbook -i hosts deploy.yml
```

**mysql**

* usuario
```html
root
```
* password
```html
mpwarg2
```
* Ejemplo conexion por consola
```html
mysql --user=root --password=mpwarg2
```

**Configurar MongoDb en máquina repository**

```html
sudo mkdir -p /data/db/
sudo chown `id -u` /data/db
```
*Modificar la configuración para permitir conexiones externas*
```html
sudo vim /etc/mongod.conf 
```
modificar la linea de Ip y poner:
```html
bindIp: 0.0.0.0 
```
Reiniciar servidor MongoDb
```html
sudo service mongod restart 
```

**Instalar MongoDb en máquina worker para php**
*añadir php.ini*
```html
extension=mongodb.so
```
Reiniciar servidor apache
```html
sudo service apache2 restart 
```
*Añadir datos maestros*
```html
cd /var/www/html/meteoSalle
sudo apt-get install mongodb-clients
mongoimport --host 172.31.46.241 -d meteoSalle -c AemetStation --type csv --file DatosBase/AEMET_ListadoEstaciones.csv --headerline
mongoimport --host 172.31.46.241 -d meteoSalle -c Town --type csv --file DatosBase/Municipios.csv --headerline
mongoimport --host 172.31.46.241 -d meteoSalle -c Province --type csv --file DatosBase/Provincias.csv --headerline

```
**Config Symfony**

```html
sudo vim /etc/apache2/sites-enabled/000-default.conf
```

*Cambiar el DocumentRoot:*

```html
DocumentRoot /var/www/html/meteoSalle/web
```

*Añadir lo siguiente:*

```html
<Directory /var/www/html/meteoSalle/web >
        FallbackResource /app_dev.php
</Directory>
```

*Reiniciar Apache:*

```html
sudo service apache2 restart
```
**Lanzar actualización de datos de proveedores desde concola**

Acceder a siguiente ruta:
```html
cd /var/www/html/meteoSalle
```
Ejecutar el comando para datos de prevision
```html
php bin/console app:GetDataProviderCommand
```

Ejecutar el comando para datos de historico
```html
php bin/console app:GetDataProviderCommand -H
```

Ejecutar el comando para cargar datos historicos específicos
```html
php bin/console app:GetDataProviderCommand -H -B 2017-06-01 -E 2017-07-01
```

**Llamadas a Api para acceso a datos de proveedores ya tratados**

*Llamada para los datos históricos de un municipio del mes indicado*
```html
apiDataProvider/getHistory/Provincia/Municipio/Año/Mes
http://52.51.95.227/apiDataProvider/getHistory/08/019/2017/06
```

*Llamada para los datos históricos de un municipio a una fecha dada*
```html
apiDataProvider/getHistory/Provincia/Municipio/Fecha
http://52.51.95.227/apiDataProvider/getHistory/08/019/2017-06-10
```

*Llamada para todos los datos de previsión a un día dado con todos los municipios*
```html
apiDataProvider/getAllPredictions/ES/Fecha
http://52.51.95.227/apiDataProvider/getAllPredictions/ES/2017-06-23
```

*Llamada para los datos de previsión de un municipio a un día dado*
```html
apiDataProvider/getPredictionsTown/Provincia/Municipio/Fecha
http://52.51.95.227/apiDataProvider/getPredictionsTown/08/019/2017-08-11
```

**Añadir tarea programada a servidor para actualizar los datos historicos y predicciones**
en consola acceder a la configuración de cronab con:
```html
crontab -e
```
Añadir las siguientes líneas al archivo:
```html
0,15,30,50 * * * * sudo php /var/www/meteoSalle/current/meteoSalle/bin/console app:GetDataProviderCommand --env=dev
* 1 * * * sudo php /var/www/meteoSalle/current/meteoSalle/bin/console app:GetDataProviderCommand -H --env=dev
```

