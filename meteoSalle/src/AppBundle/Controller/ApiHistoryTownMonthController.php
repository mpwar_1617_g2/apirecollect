<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use AppBundle\Domine\Service\PredictionForDays;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\FOSRestBundle;
use \DateTime;
use AppBundle\Application\Service\HistoryFindByTownAndMonthUseCase;
use AppBundle\Application\Service\HistoryFindByTownAndDateUseCase;
use AppBundle\Controller\Repository\RepositoryMongoDb;

class ApiHistoryTownMonthController extends BaseController
{
    /**
     *
     * @Get("/{province}/{town}/{year}/{month}", name="_list")
     *
     * @param Request $request
     * @param int $province
     * @param int $town
     * @param int $year
     * @param int $month
     * @return JsonResponse
     */
    public function getHistoryAction(Request $request, int $province, int $town, int $year, int $month)
    {
        return $this->findHistory($province, $town, $year, $month);
    }

    private function findHistory(int $province, int $town, int $year, int $month)
    {
        $repositoryMongoDb = new RepositoryMongoDb($this->get('doctrine_mongodb'));

        $historyFindByTownAndDateUseCase = new HistoryFindByTownAndDateUseCase($repositoryMongoDb);
        $historyFindByTownAndMonthUseCase = new HistoryFindByTownAndMonthUseCase(
            $repositoryMongoDb,
            $historyFindByTownAndDateUseCase);
        $serializedHistory = $historyFindByTownAndMonthUseCase($province, $town, $year ,$month);

        return new JsonResponse($serializedHistory);
    }
}