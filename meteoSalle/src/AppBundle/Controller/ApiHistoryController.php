<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use AppBundle\Domine\Service\PredictionForDays;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\FOSRestBundle;
use \DateTime;
use AppBundle\Application\Service\HistoryFindByTownAndDateUseCase;
use AppBundle\Controller\Repository\RepositoryMongoDb;

class ApiHistoryController extends BaseController
{
    /**
     *
     * @Get("/{province}/{town}/{date}", name="_list2")
     *
     * @param Request $request
     * @param int $province
     * @param int $town
     * @param string $date
     * @return JsonResponse
     */
    public function getHistoryAction(Request $request, int $province, int $town, DateTime $date)
    {
        return $this->findHistory($province, $town, $date);
    }

    private function findHistory(int $province, int $town, DateTime $date)
    {
        $repositoryMongoDb = new RepositoryMongoDb($this->get('doctrine_mongodb'));

        $historyFindByTownAndDateUseCase = new HistoryFindByTownAndDateUseCase($repositoryMongoDb);
        $serializedHistory = $historyFindByTownAndDateUseCase($province, $town, $date);

        return new JsonResponse($serializedHistory);
    }
}