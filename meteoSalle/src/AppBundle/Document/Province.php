<?php

namespace AppBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use \DateTime;

/**
 * @MongoDB\Document
 */
class Province
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="int")
     */
    private $province;

    /**
     * @MongoDB\Field(type="string")
     */
    private $name;

    /**
     * @MongoDB\Field(type="int")
     */
    private $townCapital;

    /**
     * @MongoDB\Field(type="date")
     */
    private $dateUpdate;

    private $repository;

    public function getId()
    {
        return $this->id;
    }

    public function setProvince($province)
    {
        $this->province = $province;

    }
    public function getProvince()
    {
        return $this->province;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setTownCapital($townCapital)
    {
        $this->townCapital = $townCapital;
    }
    public function getTownCapital()
    {
        return $this->townCapital;
    }

    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    }
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    public function __construct()
    {
    }

    static function create():Province{
        $province = new Province();
        return $province;
    }
}
