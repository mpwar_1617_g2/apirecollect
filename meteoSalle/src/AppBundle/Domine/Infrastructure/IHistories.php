<?php

namespace AppBundle\Domine\Infrastructure;
use AppBundle\Document\History;

interface IHistories
{
    public function count():int;

    public function rewind();

    public function valid():bool;

    public function current():History;

    public function next();
}