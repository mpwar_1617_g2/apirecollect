<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use AppBundle\Domine\Service\PredictionForDays;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\FOSRestBundle;
use \DateTime;
use AppBundle\Application\Service\PredictionFindByCountryAndDateUseCase;
use AppBundle\Controller\Repository\RepositoryMongoDb;

class ApiPredictionController extends BaseController
{
    /**
     *
     * @Get("/{country}/{date}", name="_list")
     *
     * @param Request $request
     * @param string $country
     * @param string $date
     * @return JsonResponse
     */
    public function getAllPredictionsAction(Request $request, string $country, DateTime $date)
    {
        return $this->findAllPredictions($country, $date);
    }

    private function findAllPredictions(string $country ,DateTime $date)
    {
        $repositoryMongoDb = new RepositoryMongoDb($this->get('doctrine_mongodb'));
        $predictionFindByCountryAndDateUseCase = new PredictionFindByCountryAndDateUseCase($repositoryMongoDb);
        $serializedPredictions = $predictionFindByCountryAndDateUseCase($country ,$date);

        return new JsonResponse($serializedPredictions);
    }
}
