<?php

namespace AppBundle\Domine\Infrastructure;
use AppBundle\Domine\Model\Date;
use AppBundle\Domine\Infrastructure\IPredictions;
use AppBundle\Domine\Infrastructure\IHistories;
use \DateTime;
use AppBundle\Document\PredictionDay;
use AppBundle\Document\History;
use AppBundle\Document\Province;
use AppBundle\Document\Town;

interface IRepository
{
    public function findPredictionDayWithTownAndDate(string $townCode, DateTime $date):IPredictions;

    public function findPredictionDayWithCountryAndDate(string $country, DateTime $date):IPredictions;

    public function findHistoryWithTownAndDate(string $townCode, DateTime $date):IHistories;

    public function findHistoryWithProvinceAndDate(string $province, DateTime $date):IHistories;

    public function findProvinceByCode(string $code);

    public function findAllProvincesForPrediction();

    public function findTownByName(string $name);

    public function findTownByProvinceAndCode(int $province, int $code);

    public function findTownsForPrediction();

    public function removePredictionDay(DateTime $date, string $provider, string $town);

    public function savePrediction(PredictionDay $predictionDay);

    public function saveHistory(History $history);

    public function saveProvince(Province $province);

    public function saveTown(Town $town);

    public function findAemetStationByName(string $name);

    public function findAemetStationByCode(string $code);

    public function removeHistoryByDay(Date $date, string $provider, string $town);
}