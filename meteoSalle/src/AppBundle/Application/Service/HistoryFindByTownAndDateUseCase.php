<?php

namespace AppBundle\Application\Service;
use \DateTime;
use \DateInterval;
use AppBundle\Document\Town;
use AppBundle\Domine\Infrastructure\IRepository;
use AppBundle\Domine\Service\SerializeHistory;

class HistoryFindByTownAndDateUseCase
{
    private $repository;

    public function __construct(IRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(int $provinceCode, int $townCode, DateTime $date):array
    {
        $townSearch = $this->repository->findTownByProvinceAndCode($provinceCode, $townCode);

        $histories = $this->searchHistoryes($provinceCode, $townSearch->getCodeWithProvincie(), $date);
        $serializeHistory = new SerializeHistory();
        $historyFinded = $serializeHistory->__invoke($histories, $townSearch->getCodeWithProvincie(), $date);

        return $historyFinded;
    }

    private function searchHistoryes(int $provinceCode, string $townCode, DateTime $date)
    {
        $numberOfTry = 4;
        $actualTry = 0;
        $actualDate = clone $date;

        $histories = $this->repository->findHistoryWithTownAndDate($townCode, $actualDate);

        while ($actualTry !== $numberOfTry && $histories->count() === 0)
        {
            $dateInterval = new DateInterval('P1D');
            $dateInterval->invert = 1;
            $actualDate->add($dateInterval);
            $actualTry ++;

            $histories = $this->repository->findHistoryWithTownAndDate($townCode, $actualDate);
        }

        if ($histories->count() === 0)
        {
            $histories = $this->repository->findHistoryWithProvinceAndDate($provinceCode, $date);
        }

        return $histories;
    }
}