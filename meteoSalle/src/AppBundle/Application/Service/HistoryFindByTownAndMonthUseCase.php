<?php

namespace AppBundle\Application\Service;
use AppBundle\Controller\Repository\Histories;
use \DateTime;
use \DateInterval;
use AppBundle\Domine\Infrastructure\IRepository;
use AppBundle\Application\Service\HistoryFindByTownAndDateUseCase;

class HistoryFindByTownAndMonthUseCase
{
    private $repository;
    private $historyFindByTownAndDateUseCase;

    public function __construct
    (
        IRepository $repository,
        HistoryFindByTownAndDateUseCase $historyFindByTownAndDateUseCase
    )
    {
        $this->repository = $repository;
        $this->historyFindByTownAndDateUseCase = $historyFindByTownAndDateUseCase;
    }

    public function __invoke(int $provinceCode, int $townCode, int $year, int $month):array
    {
        $dateActual = new DateTime ("$year-$month-01");
        $dateTo = new DateTime ("$year-$month-01");
        $intervalTo = new DateInterval('P1M');
        $dateTo->add($intervalTo);
        $historyesFinded = array();

        while($dateActual < $dateTo) {
            $historyes = $this->historyFindByTownAndDateUseCase->__invoke($provinceCode, $townCode, $dateActual);

            if ($historyes["status"] !== 404){
                $historyesFinded [] = $historyes;
            }

            $dateActual->add(new DateInterval('P1D'));
        }

        return $historyesFinded;
    }
}