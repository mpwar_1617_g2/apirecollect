<?php

namespace AppBundle\Application\Service;
use \DateTime;
use AppBundle\Domine\Service\SerializePredictions;
use AppBundle\Domine\Infrastructure\IRepository;

class PredictionFindByCountryAndDateUseCase
{
    private $repository;

    public function __construct(IRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(string $country, DateTime $date):array
    {
        $predictions = $this->repository->findPredictionDayWithCountryAndDate($country, $date);
        $serializePredictions = new SerializePredictions();
        $serializedPredictions = $serializePredictions->__invoke($predictions);
        return $serializedPredictions;
    }
}