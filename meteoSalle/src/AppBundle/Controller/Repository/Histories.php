<?php

namespace AppBundle\Controller\Repository;
use AppBundle\Domine\Model\Date;
use Doctrine\MongoDB\CursorInterface;
use \Doctrine\ODM\MongoDB\Cursor;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\UnitOfWork;
use Respect\Validation\Rules\DateTest;
use AppBundle\Document\History;
use AppBundle\Domine\Infrastructure\IHistories;

class Histories
    extends Cursor
    implements IHistories
{
    private $cursor;

    public function __construct(Cursor $cursor)
    {
        $this->cursor = $cursor;
    }

    public function count():int
    {
        return count($this->cursor);
    }

    public function rewind()
    {
        $this->cursor->rewind();
    }

    public function valid():bool
    {
        return $this->cursor->valid();
    }

    public function current():History
    {
        return $this->cursor->current();
    }

    public function next()
    {
        return $this->cursor->next();
    }
}