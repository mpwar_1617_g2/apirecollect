<?php

namespace AppBundle\Domine\Infrastructure;

interface DataProvider
{
    public function getName():string;
}