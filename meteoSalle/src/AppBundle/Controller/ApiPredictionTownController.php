<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use AppBundle\Domine\Service\PredictionForDays;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Options;
use FOS\RestBundle\FOSRestBundle;
use \DateTime;
use AppBundle\Application\Service\PredictionFindByTownAndDateUseCase;
use AppBundle\Application\Service\FindPredictionTownUseCase;
use GuzzleHttp\Client as ApiClient;
use AppBundle\Controller\Repository\RepositoryMongoDb;

class ApiPredictionTownController extends BaseController
{
    /**
     *
     * @Get("/{province}/{town}/{date}", name="_list")
     *
     * @param Request $request
     * @param int $province
     * @param int $town
     * @param string $date
     * @return JsonResponse
     */
    public function getPredictionAction(Request $request, int $province, int $town, DateTime $date)
    {
        return $this->getPrediction($province, $town, $date);
    }

    private function getPrediction(int $province, int $townCode, DateTime $date)
    {

        $repositoryMongoDb = new RepositoryMongoDb ($this->get('doctrine_mongodb'));
        $apiClient = new ApiClient();

        $predictionFindByTownAndDateUseCase = new PredictionFindByTownAndDateUseCase($repositoryMongoDb);
        $serializedPredictions = $predictionFindByTownAndDateUseCase($province, $townCode, $date);

        if (array_key_exists("status", $serializedPredictions))
        {
            $town = $repositoryMongoDb->findTownByProvinceAndCode($province, $townCode);

            $findPredictionTownUseCase = new FindPredictionTownUseCase($repositoryMongoDb, $apiClient);
            $findPredictionTownUseCase($town , false);

            if ($findPredictionTownUseCase){
                $serializedPredictions = $predictionFindByTownAndDateUseCase($province, $townCode, $date);
            }
        }

        return new JsonResponse($serializedPredictions);
    }
}