<?php
namespace AppBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use \DateTime;
use \Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Domine\Model\SkyStatus;

/**
 * @MongoDB\Document
 */
class PredictionDay
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="date")
     */
    private $date;

    /**
     * @MongoDB\Field(type="string")
     */
    private $provider;

    /**
     * @MongoDB\Field(type="string")
     */
    private $country;

    /**
     * @MongoDB\Field(type="string")
     */
    private $town;

    /**
     * @MongoDB\Field(type="int")
     */
    private $temperatureMinimum;

    /**
     * @MongoDB\Field(type="int")
     */
    private $temperatureMaximum;

    /**
     * @MongoDB\Field(type="int")
     */
    private $temperatureEarlyMorning;

    /**
     * @MongoDB\Field(type="int")
     */
    private $temperatureMorning;

    /**
     * @MongoDB\Field(type="int")
     */
    private $temperatureAfternoon;

    /**
     * @MongoDB\Field(type="int")
     */
    private $temperatureEvening;

    /**
     * @MongoDB\Field(type="int")
     */
    private $rainProbabilityEarlyMorning;

    /**
     * @MongoDB\Field(type="int")
     */
    private $rainProbabilityMorning;

    /**
     * @MongoDB\Field(type="int")
     */
    private $rainProbabilityAfternoon;

    /**
     * @MongoDB\Field(type="int")
     */
    private $rainProbabilityEvening;

    /**
     * @MongoDB\Field(type="string")
     */
    private $skyStatusEarlyMorning;

    /**
     * @MongoDB\Field(type="string")
     */
    private $skyStatusMorning;

    /**
     * @MongoDB\Field(type="string")
     */
    private $skyStatusAfternoon;

    /**
     * @MongoDB\Field(type="string")
     */
    private $skyStatusEvening;

    /**
     * @MongoDB\Field(type="int")
     */
    private $windMaximum;

    /**
     * @MongoDB\ReferenceMany(targetDocument="PredictionHour", cascade={"persist", "remove"})
     */
    private $hour = array();

    /**
     * @MongoDB\Field(type="date")
     */
    private $dateUpdate;

    public function getId()
    {
        return $this->id;
    }

    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setProvider($provider)
    {
        $this->provider = $provider;
    }
    public function getProvider()
    {
        return $this->provider;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }
    public function getCountry()
    {
        return $this->country;
    }

    public function setTown($town)
    {
        $this->town = $town;
    }
    public function getTown()
    {
        return $this->town;
    }

    public function setTemperatureMinimum($temperatureMinimum)
    {
        $this->temperatureMinimum = $temperatureMinimum;
    }
    public function getTemperatureMinimum()
    {
        return $this->temperatureMinimum;
    }

    public function setTemperatureMaximum($temperatureMaximum)
    {
        $this->temperatureMaximum = $temperatureMaximum;
    }
    public function getTemperatureMaximum()
    {
        return $this->temperatureMaximum;
    }

    public function setTemperatureEarlyMorning(int $temperatureEarlyMorning)
    {
        $this->temperatureEarlyMorning = $temperatureEarlyMorning;
    }
    public function getTemperatureEarlyMorning()
    {
        return $this->temperatureEarlyMorning;
    }

    public function setTemperatureMorning(int $temperatureMorning)
    {
        $this->temperatureMorning = $temperatureMorning;
    }
    public function getTemperatureMorning()
    {
        return $this->temperatureMorning;
    }

    public function setTemperatureAfternoon(int $temperatureAfternoon)
    {
        $this->temperatureAfternoon = $temperatureAfternoon;
    }
    public function getTemperatureAfternoon()
    {
        return $this->temperatureAfternoon;
    }

    public function setTemperatureEvening(int $temperatureEvening)
    {
        $this->temperatureEvening = $temperatureEvening;
    }
    public function getTemperatureEvening()
    {
        return $this->temperatureEvening;
    }

    public function setRainProbabilityEarlyMorning(int $rainProbabilityEarlyMorning)
    {
        $this->rainProbabilityEarlyMorning = $rainProbabilityEarlyMorning;
    }
    public function getRainProbabilityEarlyMorning():int
    {
        return $this->rainProbabilityEarlyMorning;
    }

    public function setRainProbabilityMorning(int $rainProbabilityMorning)
    {
        $this->rainProbabilityMorning = $rainProbabilityMorning;
    }
    public function getRainProbabilityMorning():int
    {
        return $this->rainProbabilityMorning;
    }

    public function setRainProbabilityAfternoon(int $rainProbabilityAfternoon)
    {
        $this->rainProbabilityAfternoon = $rainProbabilityAfternoon;
    }
    public function getRainProbabilityAfternoon():int
    {
        return $this->rainProbabilityAfternoon;
    }

    public function setRainProbabilityEvening(int $rainProbabilityEvening)
    {
        $this->rainProbabilityEvening = $rainProbabilityEvening;
    }
    public function getRainProbabilityEvening():int
    {
        return $this->rainProbabilityEvening;
    }

    public function setSkyStatusEarlyMorning(SkyStatus $skyStatusEarlyMorning)
    {
        $this->skyStatusEarlyMorning = $skyStatusEarlyMorning;
    }
    public function getSkyStatusEarlyMorning():SkyStatus
    {
        return new SkyStatus($this->skyStatusEarlyMorning);
    }

    public function setSkyStatusMorning(SkyStatus $skyStatusMorning)
    {
        $this->skyStatusMorning = $skyStatusMorning;
    }
    public function getSkyStatusMorning():SkyStatus
    {
        return new SkyStatus($this->skyStatusMorning);
    }

    public function setSkyStatusAfternoon(SkyStatus $skyStatusAfternoon)
    {
        $this->skyStatusAfternoon = $skyStatusAfternoon;
    }
    public function getSkyStatusAfternoon():SkyStatus
    {
        return new SkyStatus($this->skyStatusAfternoon);
    }

    public function setSkyStatusEvening(SkyStatus $skyStatusEvening)
    {
        $this->skyStatusEvening = $skyStatusEvening;
    }
    public function getSkyStatusEvening():SkyStatus
    {
        return new SkyStatus($this->skyStatusEvening);
    }

    public function setWindMaximum(int $windMaximum)
    {
        $this->windMaximum = $windMaximum;
    }
    public function getWindMaximum():int
    {
        return $this->windMaximum;
    }

    public function addHour(\AppBundle\Document\PredictionHour $hour)
    {
        $this->hour[] = $hour;
    }

    public function removeHour(\AppBundle\Document\PredictionHour $hour)
    {
        $this->hour->removeElement($hour);
    }

    public function getHour()
    {
        return $this->hour;
    }

    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    }

    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    public function __construct()
    {
        $this->hour = new ArrayCollection();
    }
}
