<?php

namespace AppBundle\Domine\Model;
use \InvalidArgumentException;

class SkyStatus
{
    private $skyStatus;
    const VALID_VALUES = array(
                        "sun" => "sun",
                        "Despejado" => "sun",
                        "Poco nuboso" => "sun",
                        "Intervalos nubosos" => "sun",
                        "Nubes altas" => "sun",
                        "cloud" => "cloud",
                        "Bruma" => "cloud",
                        "Niebla" => "cloud",
                        "Calima" => "cloud",
                        "Cubierto con lluvia escasa" => "cloud",
                        "Nuboso" => "cloud",
                        "Muy nuboso" => "cloud",
                        "Cubierto" => "cloud",
                        "Muy nuboso con lluvia escasa" => "cloud",
                        "Nuboso con lluvia escasa" => "cloud",
                        "Intervalos nubosos con lluvia escasa" => "cloud",
                        "Muy nuboso con tormenta y lluvia escasa" => "cloud",
                        "Nuboso con tormenta y lluvia escasa" => "cloud",
                        "rain" => "rain",
                        "Intervalos nubosos con tormenta y lluvia escasa" => "rain",
                        "Muy nuboso con tormenta" => "rain",
                        "Intervalos nubosos con tormenta" => "rain",
                        "Intervalos nubosos con lluvia" => "rain",
                        "Nuboso con lluvia" => "rain",
                        "Muy nuboso con lluvia" => "rain",
                        "Cubierto con lluvia" => "rain",
                        "Chubascos" => "rain",
                        "storm" => "storm",
                        "Tormenta" => "storm",
                        "Granizo" => "storm",
                        "snow" => "snow",
                        "Nuboso con tormenta" => "cloud",
                        "Intervalos nubosos con nieve" => "snow",
                        "Nuboso con nieve" => "snow",
                        "Muy nuboso con nieve" => "snow",
                        "Cubierto con nieve" => "snow",
                        "Cubierto con tormenta" => "storm",
                        );

    public function __construct
    (
        string $skyStatus
    )
    {
        if (!SkyStatus::validate($skyStatus)) {
            throw new InvalidArgumentException(sprintf('"%s" invalid Sky Status. use SkyStatus::translateStatusToStandard() for translate sky status.', $skyStatus));
        }

        $this->skyStatus = SkyStatus::translateStatusToStandard($skyStatus);
    }

    static function validate(String $skyStatus):bool{
        if (array_key_exists($skyStatus, SkyStatus::VALID_VALUES)) {
            return true;
        }
        return false;
    }

    static function translateStatusToStandard(string $skyStatusToTranslate):string{
        if (array_key_exists($skyStatusToTranslate, SkyStatus::VALID_VALUES)) {
            return SkyStatus::VALID_VALUES[$skyStatusToTranslate];
        }
    }

    public function __toString():string
    {
        return $this->skyStatus;
    }

    public function equals(SkyStatus $skyStatus):bool
    {
        return strtolower((string) $this) === strtolower((string) $skyStatus);
    }
}