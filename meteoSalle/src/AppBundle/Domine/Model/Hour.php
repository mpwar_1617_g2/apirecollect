<?php

namespace AppBundle\Domine\Model;
use \InvalidArgumentException;

class Hour
{
    private $hour;
    const HOUR_MINIMUN = 0;
    const HOUR_MAXIMUN = 23;

    public function __construct
    (
        int $hour
    )
    {
        if (!Hour::validate($hour)) {
            throw new InvalidArgumentException(sprintf('"%s" invalid hour. The hour must be between 0 to 23', $skyStatus));
        }

        $this->hour = $hour;
    }

    static function validate(String $hour):bool{
        if (filter_var($hour, FILTER_VALIDATE_INT, array("options" => array("min_range"=>Hour::HOUR_MINIMUN, "max_range"=>Hour::HOUR_MAXIMUN))) === true) {
            return true;
        }

        return false;
    }

    public function __toString():string
    {
        return $this->hour;
    }

    public function equals(Hour $hour):bool
    {
        return strtolower((string) $this) === strtolower((string) $hour);
    }
}