<?php
/**
 * Created by PhpStorm.
 * User: racso
 * Date: 1/09/17
 * Time: 19:04
 */

namespace AppBundle\Controller\Repository;
use AppBundle\AppBundle;
use AppBundle\Domine\Infrastructure\IHistories;
use AppBundle\Domine\Infrastructure\IRepository;
use AppBundle\Domine\Infrastructure\IPredictions;
use AppBundle\Domine\Model\Date;
use \DateTime;
use \DateInterval;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use AppBundle\Controller\Repository\Predictions;
use AppBundle\Controller\Repository\Histories;
use AppBundle\Document\PredictionDay;
use AppBundle\Document\History;
use AppBundle\Document\Town;
use AppBundle\Document\Province;

class RepositoryMongoDb
    implements IRepository
{
    private const documentPredictionDay = 'AppBundle:PredictionDay';
    private const documentHistory = 'AppBundle:History';
    private const documentProvince = 'AppBundle:Province';
    private const documentTown = 'AppBundle:Town';
    private const documentAemetStation = 'AppBundle:AemetStation';
    private $mongoDB;
    public function __construct(ManagerRegistry $mongoDB)
    {
        $this->mongoDB = $mongoDB;
    }

    public function findPredictionDayWithTownAndDate(string $townCode, DateTime $date):IPredictions
    {
        $cursorPredictions = $this->mongoDB
            ->getManager()
            ->createQueryBuilder($this::documentPredictionDay)
            ->field('town')->equals($townCode)
            ->field('date')->equals($date)
            ->getQuery()
            ->execute();

        return new Predictions($cursorPredictions);
    }

    public function findPredictionDayWithCountryAndDate(string $country, DateTime $date):IPredictions
    {
        $cursorPredictions = $this->mongoDB
            ->getManager()
            ->createQueryBuilder($this::documentPredictionDay)
            ->field('country')->equals($country)
            ->field('date')->equals($date)
            ->sort('town','ASC')
            ->getQuery()
            ->execute();

        return new Predictions($cursorPredictions);
    }

    public function findHistoryWithTownAndDate(string $townCode, DateTime $date):IHistories
    {
        $cursorHistories = $this->mongoDB
            ->getManager()
            ->createQueryBuilder($this::documentHistory)
            ->field('town')->equals($townCode)
            ->field('date')->equals($date)
            ->getQuery()
            ->execute();

        return new Histories($cursorHistories);
    }

    public function findHistoryWithProvinceAndDate(string $province, DateTime $date):IHistories
    {
        $numberOfTry = 4;
        $actualTry = 0;
        $actualDate = clone $date;

        $cursorHistories = $this->mongoDB
            ->getManager()
            ->createQueryBuilder($this::documentHistory)
            ->field('province')->equals($province)
            ->field('date')->equals($actualDate)
            ->getQuery()
            ->execute();

        while ($actualTry !== $numberOfTry && count($cursorHistories) === 0)
        {
            $cursorHistories = $this->mongoDB
                ->getManager()
                ->createQueryBuilder($this::documentHistory)
                ->field('province')->equals($province)
                ->field('date')->equals($actualDate)
                ->getQuery()
                ->execute();

            $dateInterval = new DateInterval('P1D');
            $dateInterval->invert = 1;
            $actualDate->add($dateInterval);
            $actualTry ++;
        }

        return new Histories($cursorHistories);
    }

    public function findProvinceByCode(string $code)
    {
        return $this->mongoDB
            ->getRepository($this::documentProvince)
            ->findOneBy(array('province' => $code));
    }

    public function findAllProvincesForPrediction(){
        $dateActual = new DateTime (date("Y-m-d"));

        return $this->mongoDB
            ->getManager()
            ->createQueryBuilder($this::documentProvince)
            ->field('dateUpdate')->notEqual($dateActual)
            ->getQuery()
            ->execute();
    }

    public function findTownByName(string $name)
    {
        return $this->mongoDB
            ->getRepository($this::documentTown)
            ->findOneByName($name);
    }

    public function findTownByProvinceAndCode(int $province, int $code)
    {
        return $this->mongoDB
            ->getRepository($this::documentTown)
            ->findOneBy(array('province' => $province, 'code' => $code));
    }

    public function findTownsForPrediction(){
        $dateActual = new DateTime (date("Y-m-d"));

        return $this->mongoDB
            ->getManager()
            ->createQueryBuilder($this::documentTown)
            ->field('dateUpdate')->notEqual($dateActual)
            ->getQuery()
            ->execute();
    }

    public function removePredictionDay(DateTime $date, string $provider, string $town)
    {
        $mongoDbRepository = $this->mongoDB->getManager();

        $mongoDbRepositoryes = $this->mongoDB
            ->getManager()
            ->createQueryBuilder($this::documentPredictionDay)
            ->field('date')->equals($date)
            ->field('provider')->equals($provider)
            ->field('town')->equals($town)
            ->getQuery()
            ->execute();

        foreach ($mongoDbRepositoryes as $predictionDay) {
            $mongoDbRepository->remove($predictionDay);
            $mongoDbRepository->flush();
        }
    }

    public function savePrediction(PredictionDay $predictionDay)
    {
        $repository = $this->mongoDB->getManager();
        $repository->persist($predictionDay);
        $repository->flush();
    }

    public function saveHistory(History $history)
    {
        $repository = $this->mongoDB->getManager();
        $repository->persist($history);
        $repository->flush();
    }

    public function saveTown(Town $town)
    {
        $repository = $this->mongoDB->getManager();
        $repository->persist($town);
        $repository->flush();
    }

    public function saveProvince(Province $province)
    {
        $repository = $this->mongoDB->getManager();
        $repository->persist($province);
        $repository->flush();
    }

    public function findAemetStationByName(string $name){
        return $this->mongoDB
            ->getRepository($this::documentAemetStation)
            ->findOneByName($name);
    }

    public function findAemetStationByCode(string $code){
        return $this->mongoDB
            ->getRepository($this::documentAemetStation)
            ->findOneByCode($code);
    }

    public function removeHistoryByDay(Date $date, string $provider, string $town){
        $historyes = $this->mongoDB
            ->getManager()
            ->createQueryBuilder($this::documentHistory)
            ->field('date')->equals($date)
            ->field('provider')->equals($provider)
            ->field('town')->equals($town)
            ->getQuery()
            ->execute();

        foreach ($historyes as $history) {
            $this->saveHistory($history);
        }
    }
}